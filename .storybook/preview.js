import React from 'react';
import { addons } from '@storybook/addons';
import { LIVE_EXAMPLES_ADDON_ID } from 'storybook-addon-live-examples';
import theme from 'prism-react-renderer/themes/vsDark';
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql,
  DefaultOptions,
  useLazyQuery
} from "@apollo/client";
import ReactJson from 'react-json-view';

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  options: {
    storySort: {
      order: ['GraphQL', ['Setup', 'Accounts', 'Markets', 'Liquidity Pools']], 
    },
  }
}

/**
 * Setup Apollo Client
 */
 const client = new ApolloClient({
  uri: 'https://n22.hg.network/subgraphs/name/fildaio/heco',
  cache: new InMemoryCache()
});

const App = ({Component}) => {
  return (
    <ApolloProvider client={client}>
      <Component />
    </ApolloProvider>
  )
}

addons.setConfig({
  [LIVE_EXAMPLES_ADDON_ID]: {
      // custom theme from prism-react-renderer (optional)
      editorTheme: theme,
      // internationalization (optional)
      copyText: ['Copy', 'Copied'],
      expandText: ['Show code', 'Hide code'],
      shareText: ['Share', 'Shared'],
      scope: {
        value: 2 ,
        ApolloClient,
        InMemoryCache,
        ApolloProvider,
        useQuery,
        gql,
        DefaultOptions,
        useLazyQuery,
        ReactJson,
        App
      }
  },
});